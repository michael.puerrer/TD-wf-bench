#!/bin/bash

# Figure out how many cores to run on
NPROCS=$@

if [ -z "$NPROCS" ]; then
  # Get number of cores (should work on Linux and OS X)
  NCORES=`getconf _NPROCESSORS_ONLN`
  NPROCS=$((NCORES / 2)) # divide by two for safety
  echo "Running on maximum available number of cores for this machine: $NPROCS.";
else 
  echo "Running on $NPROCS cores.";
fi

CFG=./wf_bench.json
LOG=./wf_bench_$NPROCS.log
APP=Generate_wf_MPI.py

time mpirun -n ${NPROCS} ${APP} -o ${CFG} > ${LOG} 2>&1
grep "Waveform generation took" wf_bench_$NPROCS.log
rm -rf ./wf_bench_out/tmp
mv ./wf_bench_out ./wf_bench_out_$NPROCS

